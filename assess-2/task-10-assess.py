# 10. Имеется файл file.txt с текстом. Напишите программу, которая выводит следующую статистику по тексту:
# ● количество букв латинского алфавита;
# ● число строк.

# alphabet = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']
# count_letter = 0
#
# with open('file.txt') as file_text:
#     count_lines = len(file_text.readlines())
# with open('file.txt') as file_text:
#     for i in file_text.read().lower():
#         if i in alphabet:
#             count_letter += 1
#
# print(f'Количество букв латинского алфавита: {count_letter}')
# print(f'Число строк: {count_lines}')


alphabet = 'abcdefghijklmnopqrstuvwxyz'
count_letter = 0

with open('file.txt') as file_text:
    for line in file_text.readlines():
        count_lines += 1
        for i in line.lower():
            if i in alphabet:
                count_letter += 1

print(f'Количество букв латинского алфавита: {count_letter}')
print(f'Число строк: {count_lines}')