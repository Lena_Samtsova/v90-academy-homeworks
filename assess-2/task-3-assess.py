# ok 3. Дана строка в виде случайной последовательности чисел от 0 до 9.
# Требуется создать словарь, который в качестве ключей будет принимать данные числа
# (т. е. ключи будут типом int), а в качестве значений – количество этих чисел в имеющейся последовательности.
# На экран вывести словарь из 3-х самых часто встречаемых чисел.

str_dig = input('Введите случайную последовательности чисел от 0 до 9: ')
# dict_dig = {int(i): str_dig.count(i) for i in str_dig}
# sorted_dict_dig = sorted(dict_dig.items(), key=lambda element: element[1])
# print(f'словарь из 3-х самых часто встречаемых чисел: {sorted_dict_dig[-3:]}')

result = {}
for i in str_dig:
    i = int(i)
    if i not in result:
        result[i] = 1
    else:
        result[1] += 1
print(result)