# ok 5. Необходимо написать программу, которая проверяет,
# существует ли значение в заданном словаре.
input_val = int(input('Введите значение для проверки его наличия: '))
dict_have = {'Red wine': 25, 'White wine': 30, 'Champagne': 35, 'Liqueur': 20, 'Tequila': 25, 'Absinthe':35}
if input_val in dict_have.values():
    print(f'Значение {input_val} есть в данном словаре.')
else:
    print(f'Значения {input_val} в данном словаре нет.')
