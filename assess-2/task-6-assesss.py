# ok 6. Дан словарь, содержащий в качестве ключей названия коктейлей и в качестве значений – их цену.
# Пользователь указывает определенную сумму, и программа создаёт и выводит на экран список коктейлей,
# цена которых меньше введенного значения.

cocktails_dict = {'Red wine': 25, 'White wine': 30, 'Champagne': 35, 'Liqueur': 20, 'Tequila': 25, 'Absinthe':35}
while True:
    try:
        money_input = int(input('Желаемая цена за коктейль: '))
    except:
        print('Вы не ввели сумму за коктейль. Попробуйте еще раз!')
        continue
    else:
        print(f'Ваша сумма: {money_input}')
    break
if money_input == 0:
    print('Вы предложили 0 за коктейль. У нас нет бесплатных коктейлей!')
else:
    possible_cocktails = {}
    for key, value in cocktails_dict.items():
        if money_input < min(cocktails_dict.values()):
            print(f'Нет доступных коктейлей, выберите другой напиток!')
        elif money_input >= value:
            possible_cocktails.update({key: value})
    print(f'Вым доступны данные коктейли: {possible_cocktails}')


