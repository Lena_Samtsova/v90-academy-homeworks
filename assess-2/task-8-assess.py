# 8. Даны два списка чисел, которые могут содержать до 10000 чисел каждый.
# Выведите все числа, которые входят как в первый,
# так и во второй список в порядке возрастания.

list_1 = [1, 20, 3, 1, 20, 3, 4]
list_2 = [1, 3, 4, 3, 4]
list_all = []
for i in list_1:
    if i in list_all:
        continue
    elif i in list_2:
        list_all.append(i)
print(list_all)

# почему не работает if i in list_2 and not in list_all