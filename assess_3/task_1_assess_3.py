# ok 1. Напишите программу, которая с помощью анонимной функции извлеките из списка числа,
# делимые на 15. Результат вывести на экран.

def main():
    list_lambda = [150, 15, 20, 25, 30]
    list_finish = list(filter(lambda a: not a % 15, list_lambda))
    print(list_finish)


if __name__ == '__main__':
    main()