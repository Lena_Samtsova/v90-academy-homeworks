# ok 3. Напишите программу, которая содержит вложенную функцию
# для вычисления сложения следующим образом.:
# a. Создайте внешнюю функцию, которая будет принимать два параметра, a и b.
# b. Создайте внутреннюю функцию внутри внешней функции, которая будет вычислять сложение a и b.
# c. Наконец, внешняя функция добавит к сложению 5 и вернет его.

def main():
    def input_param(a, b):
        def fun_sum():
            return a + b
        fun_sum()
        return fun_sum() + 5
    print(input_param(2, 3))


if __name__ == '__main__':
    main()