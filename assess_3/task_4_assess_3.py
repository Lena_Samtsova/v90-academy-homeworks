# ok 4. Напишите программу, которая содержит генератор calculation(),
# который принимает два параметра и вычисляет сумму и разность.
# Сперва возвращается сумма, потом разность. Результат вывести на экран.

def main():
    def calculation_generator(a, b):
        yield f'Сумма числе: {a + b}'
        yield f'Разность чисел: {a - b}'

    generator = calculation_generator(4, 3)

    for item in generator:
        print(item)


if __name__ == '__main__':
    main()