# ok 1. Напишите функцию sum_range(start, end), которая суммирует
# все целые числа от значения «start» до величины «end» включительно.
# Если пользователь задаст первое число большее чем второе, просто поменяйте их местами.
import random

def sum_range(start, end):
    if start > end:
        start, end = end, start
    sum_int = 0
    for i in range(start, end+1):
        sum_int += i
    return sum_int

def main():
    a = random.randint(0, 10)
    b = random.randint(0, 10)
    print(sum_range(a, b))

if __name__ == '__main__':
    main()

# def sum_range(start, end):
#     if start > end:
#         start, end = end, start
#     return sum(range(start, end+1))

# def sum_range(start, end):
#     return (start + end) * (abs(end - start) + 1) // 2
