# ok 3. Напишите функцию, чтобы проверить, является ли строка панграммой или нет.
# Панграмма — фраза, содержащая в себе все буквы алфавита.

def main():
    def is_pangram(text):
        if set('abcdefghijklmnopqrstuvwxyz') == set(text.lower().replace(' ', '')):
            return 'is_pangram'
        else:
            return 'is_not_pangram'

    text_input = input()
    print(is_pangram(text_input))
if __name__ == '__main__':
    main()





#     def pangramm_alf(big_string):
#         if big_string.lower() == world_5.lower()[::-1]:
#             print("Pangramm")
#         else:
#             print("It isn't pangramm")
#
# pangramm_alf(big_string)
#     big_string = 'fghjk'  # input("Введите слово:")
#     print(pangramm_alf(big_string))

