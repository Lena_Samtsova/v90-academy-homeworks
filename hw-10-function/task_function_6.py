# ok 6. Напишите генератор custom_range(start, end, step), который генерирует все целые числа
# от значения «start» до величины «end» включительно с шагом «step».
# Если пользователь задаст первое число большее чем второе, просто поменяйте их местами.
# «step» по умолчанию равен = 0. Также не допускать ввод дробных чисел.

while True:
    try:
        a = int(input('Введите целое число, начало списка: '))
        b = int(input('Введите целое число, конец списка: '))
        c = int(input('Введите целое число, шаг списка: '))
    except ValueError:
        print('Вы ввели не целое число')
        continue
    break
def custom_range(start, end, step=1):
    start -= step
    while True:
        if start > end and step < 0:
            if (start + step <= end):
                break
        else:
            if (start + step >= end):
                break
        start += step
        yield round(start, 2)

print(*custom_range(a, b, c))
