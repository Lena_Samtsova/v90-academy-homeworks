# 2. Пользователь вводит две строки. Необходимо определить, являются ли две строки анаграммами. Анаграмма — это слова состоящие из
# одних и тех же букв, расположенных в разном порядке.
world_1 = input("Введите первое слово:")
world_2 = input("Введите второе слово:")
if sorted(world_1.lower()) == sorted(world_2.lower()):
    print("Anagram")
else:
    print("It isn't anagram")

# 3. Пользователь вводит строку и символ. Определить количество вхождений символа в строку, независимо от регистра.
world_3 = input("Введите слово:")
symbol_1 = input("Введите символ:")
print(world_3.lower().count(symbol_1))

# 4. Пользователь вводит строку и символ. Необходимо определить индексы первого и последнего вхождения символа в строке.
world_4 = input("Введите слово:")
symbol_2 = input("Введите символ:")
if world_4.lower().find(symbol_2) == -1:
    print (f"Символа {symbol_2} нет в строке")
elif world_4.lower().count(symbol_2) == 1:
    print (f"Одно вхождение с индексом: {world_4.lower().index(symbol_2)}")
elif symbol_2 == "":
    print(f"Вы не указали символ!")
else:
    print(world_4.lower().find(symbol_2), world_4.lower().rfind(symbol_2))

# 5. Пользователь вводит строку. Необходимо определить, являются ли строка палиндромом, также учитывать, что строка помимо букв,
# может содержать пробел, тире, запятую, точку, восклицательный и вопросительный знак. Палиндром — слово или текст, одинаково читающееся в обоих направлениях.
world_5 = input("Введите слово:")
if world_5.lower() == world_5.lower()[::-1]:
    print("Palindrome")
else:
    print("It isn't palindrome")