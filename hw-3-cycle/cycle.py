# 1. Пользователь вводит строку и символ. Определить количество
# вхождений символа в строку, независимо от регистра, при этом нельзя использовать метод count.

# string_1 = str.lower(input('Enter the string: '))
# symbol_1 = str.lower(input("Enter the symbol: "))
# symbol_sum = 0
#
# for i in string_1:
#     if i == symbol_1:
#         symbol_sum += 1
# print(f'Number of occurrences of a character in a string: {symbol_sum}')

#2. Пользователь вводит строку и символ. Необходимо определить
# индексы первого и последнего вхождения символа в строке, при этом нельзя использовать строковые методы для поиска.
while True:
    try:
        string_2 = input('Enter the string: ')
        symbol_2 = input("Enter the symbol: ")
        index_symbol = string_2.index(symbol_2)
        index_symbol_r = string_2.rindex(symbol_2)
    except ValueError:
        print("Нужно ввести правильную строку и символ, попробуйте ещё раз ...")
        continue
    print(f'Indexes of the first and last occurrence of a character in a string: {index_symbol},{index_symbol_r}')
    break






