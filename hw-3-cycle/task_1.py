# 1. OK Дан список чисел. Вычислить сумму всех чисел: с for-циклом, с while-циклом. Результаты вывести на экран.
while True:
    try:
        list_int = list(range(int(input('Enter the number that will end the sequential list starting with 0: '))))
    except ValueError:
        print("Нужно ввести чисело. Попробуйте ещё раз...:")
        continue
        break
    else:
        print (f'list:{list_int}. Reference sum:{sum(list_int)}.')
    break
sum_list = 0
for i in list_int:
    sum_list += i # Берется последовательно каждый элемент из list_int
print (f'"for i in list_int" cycle completed. Sum:{sum_list}.')

sum_list_range = 0
for i in range(len(list_int)):
    sum_list_range += list_int[i] # Берется по индексу [i] элемент из range(len(list_int))
print (f'"for i in range(len(list_int))" cycle completed. Sum:{sum_list_range}.')

index_while = 0
sum_while = 0
while index_while < len(list_int):
    sum_while += list_int[index_while]
    index_while += 1
print (f'"while index_while < len(list_int)" cycle completed. Sum:{sum_while}.')