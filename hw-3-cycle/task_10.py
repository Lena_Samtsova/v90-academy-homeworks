# 10. Пользователь вводит строку. В строке заменить пробелы звездочкой. Если встречается подряд несколько пробелов,
# то их следует заменить одним знаком "*", пробелы в начале и конце строки удалить. Результаты вывести на экран.

string_1 = input('Enter the string: ').strip()
new_srt = string_1[0]
i = 1
while i < len(string_1):
    if string_1[i] != ' ':
        new_srt += string_1[i]
    elif string_1[i - 1] != ' ':
        new_srt += '*'
    i += 1
print(new_srt)

# Почему так не работает?
# string_1 = input('Enter the string: ').strip()
# new_srt = string_1[0]
# for i in range(len(string_1)):
#    if string_1[i] != ' ':
#        new_srt += string_1[i]
#    elif string_1[i] != ' ':
#        new_str += '*'
#    i += 1
# print(new_srt)
