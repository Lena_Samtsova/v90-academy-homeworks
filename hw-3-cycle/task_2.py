# 2. Дано два списка, например, [1, 2, 3] и [11, 22, 33]. Необходимо создать третий список вида [1, 11, 2, 22, 3, 33]. Результаты вывести на экран.
string_1 = list(input('Enter list: '))
string_2 = list(input('Enter list: '))

index_1 = 0
index_2 = 0
sum_while = []
index_for = 1
if len(string_1) >= len(string_2):
    while index_1 + index_2 < len(string_1) + len(string_2):
        try:
            sum_while += string_1[index_1] + string_2[index_2]
        except IndexError:
            string_2[index_2] = ''
        index_1 += 1
        index_2 += 1
    print (f'"while index_1 + index_2 < len(string_1) + len(string_2):" cycle completed. Sum:{sum_while}.')
elif len(string_1) > len(string_2):
    for i in range(len(string_2)):
        string_1.insert(index_for,string_2[i])
        index_for += 1
    print(f'"for i in range(len(string_2)):" cycle completed. Sum:{string_1}.')
else:
    for i in range(len(string_1)):
        string_2.insert(index_for,string_1[i])
        index_for += 1
    print(f'"for i in range(len(string_1)):" cycle completed. Sum:{string_2}.')
