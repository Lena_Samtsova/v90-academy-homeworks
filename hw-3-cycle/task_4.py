# OK 4. Квадратное уравнение имеет вид ax2 + bx + c = 0. Пользователь вводит a, b и c.
# Необходимо вычислить значение x. Результаты вывести на экран.
# Математические подсказки:
# a. сначала вычисляют дискриминант по формуле D = b2 - 4ac
# b. ЕслиD>0,то x1=(-b+D1/2)/(2a) иx2=(-b-D1/2)/(2a)
# c. ЕслиD=0,то x=-b /(2a)
# d. Если D < 0, то корней нет.

while True:
    try:
        a = int(input('Enter a: '))
    except ValueError:
        print("Нужно число а. Попробуйте ещё раз...:")
        continue
    break
while True:
    try:
        b = int(input('Enter b: '))
    except ValueError:
        print("Нужно число b. Попробуйте ещё раз...:")
        continue
    break
while True:
    try:
        c = int(input('Enter c: '))
    except ValueError:
        print("Нужно число c. Попробуйте ещё раз...:")
        continue
    break
discriminant = b ** 2 - 4 * a * c
s = (- b + discriminant / 2) / (2 * a)
print(c)
print(f'Discriminant: {discriminant}')
if discriminant < 0:
    print(f'D < 0, no roots.')
elif discriminant > 0:
    print(f'D > 0, then x1: {(- b + discriminant / 2) / (2 * a)} , then x2: {(- b - discriminant / 2) / (2 * a)}')
else:
    print(f'D = 0, then x: {- b / (2 * a)}')
