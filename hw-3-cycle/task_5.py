# 5. Пользователь вводит число. Необходимо вычислить его факториал. Результаты вывести на экран.
# Математические подсказки: Формулу можно представить в таком виде: n! = 1 * 2 * 3 * ... * (n-2) * (n-1) * n.

num_input = int(input('Enter a number to calculate its factorial: '))
factorial_for = 1
for i in range(2, num_input + 1):
    factorial_for *= i
print(factorial_for)

factorial_while = 1
while 1 < num_input:
    factorial_while *= num_input
    num_input -= 1
print(factorial_while)
