# 8. Дан список. Реализовать сортировку пузырьком. Результаты вывести на экран.

string = list(input('Enter list: '))
print(string)
# Устанавливаем swapped в True, чтобы цикл запустился хотя бы один раз
bubble = True
while bubble:
    bubble = False
    for i in range(len(string) - 1):
        if string[i] > string[i + 1]:
                # Меняем элементы
            string[i], string[i + 1] = string[i + 1], string[i]
                # Устанавливаем swapped в True для следующей итерации
            bubble = True
print(f'List sorted by "Bubble: "{string}')