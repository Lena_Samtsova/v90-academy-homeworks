# 9. Дана строка, содержащая натуральные числа и слова. Необходимо сформировать список из чисел, содержащихся в этой строке.
# Результаты вывести на экран. Например, задана строка "abc83 cde7 1 b 24". На выходе мы должны получить список [83, 7, 1, 24].

string_1 = 'abc83 24 cde7 1 b 24'
list_int = []
digit_point = ''

for i in string_1[:]:
    if i.isdigit():
        digit_point += i
    else:
        if digit_point.isdigit():
            list_int.append(int(digit_point))
            digit_point = ''
if digit_point != '':
    list_int.append(int(digit_point))

print(f'A string containing natural numbers and wordslist_int {string_1 }. Formed a list of the numbers contained in this string{list_int}.')