# 1. Даны два кортежа. Найти элементы, которые содержаться и в первом и во втором кортеже и вывести на экран.
tuple_1 = tuple(input('Enter the first tuple: '))
tuple_2 = tuple(input('Enter the second tuple: '))
# tuple_1 = (2, 4, 5, 4)
# tuple_2 = (5, 6, 4, 7)
tuple_all = []
for i in tuple_1:
    if i not in tuple_all:
        tuple_all += [i]
print(f'These elements: {tuple_all} available in both tuples.')