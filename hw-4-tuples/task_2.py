# 2. Пользователь вводит строку. Преобразовать строку к кортежу. Подсчитать частоту встречающихся букв в строке.
# Результаты вывести на экран в формате “символ : количество вхождений”.

tuple_str = tuple(input('Enter the string: ').lower())
# tuple_str = ('a', 'b', 'c', 'd', 'a', 'b', 'c', 'd', 'a', 'b', 'c', 'd', 'a', 'b', 'd', 'a', 'b')

for i in tuple_str:
    print(f'Symbol: {i} repeated in string {tuple_str.count(i)}')

# Alphabet
Alphabet = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',"а","б","в","г","д","е","ё","ж","з","и","й","к","л","м","н","о",
            "п","р","с","т","у","ф","х","ц","ч","ш","щ","ъ","ы","ь","э","ю","я"]
for i in range(len(Alphabet)):
     if tuple_str.count(Alphabet[i]) != 0:
          print(f'Symbol alphabet: {Alphabet[i]} repeated in string {tuple_str.count(Alphabet[i])}') # {i} i-индекс переменной, {Alphabet[i]} - значение индекса переменоой