# 3. Дан кортеж с температурой за несколько дней (каждый элемент – средняя температура за день).
# Посчитать среднюю температуру в течении всех дней. Результаты вывести на экран.
temperature = tuple(map(int, input('Enter average temperatures for the last few days: ').split(' ')))
# temperature = (21, 24, 25, 24)
sum_t = 0

for i in temperature:
    sum_t += i
print(f'Average temperature per day (cycle for): {sum_t / len(temperature)}')

# function sum
print(f'Average temperature per day (function sum): {sum(temperature) / len(temperature)}')