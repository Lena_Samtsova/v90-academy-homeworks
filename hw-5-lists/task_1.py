# 1. Дан список. Необходимо удалить пустые строки из списка. Результаты вывести на экран.

stping_with_empty =['', '', 'd', 'f', 'k', '',  'g', 'h', 'j', 'k', '', '', 'g', 'h', 'j', '', '', 'h', '', 'h', 'h', 'j', 'k']
print(list(filter(None, stping_with_empty)))
# Если функция отсутствует (None), то все элементы, которые являются ложными, будут удалены.
# Пустая строка считается ложным в Python и будет отфильтроваться.
while '' in stping_with_empty:
     stping_with_empty.remove('')
print(stping_with_empty)
for i in stping_with_empty: # удаляет не все пустые строки!!! почему???
    if i == '':
        stping_with_empty.remove(i)
print(stping_with_empty)