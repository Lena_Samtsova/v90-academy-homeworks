# 10.Дан список чисел. Отсортировать числа по убыванию по алгоритму Insertion Sort. Результаты вывести на экран.

insertion_sort = list(map(int, input()))
print(insertion_sort )

for i in range(1, len(insertion_sort)):
    item_to_insert = insertion_sort[i]
    j = i - 1
    while j >= 0 and insertion_sort[j] > item_to_insert:
        insertion_sort[j + 1] = insertion_sort[j]
        j -= 1
    insertion_sort[j + 1] = item_to_insert
print(insertion_sort )
