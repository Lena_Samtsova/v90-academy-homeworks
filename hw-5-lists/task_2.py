# 2. Дан список чисел. Превратите его в список квадратов этих чисел. Результаты вывести на экран.

int_num = list(map(int, input()))
print(int_num)

index = 0
sum_num_while = []
sum_num_for = []
while index in range(len(int_num)):
    sum_num_while += [int_num[index] ** 2]
    index += 1
print(sum_num_while)

for i in range(len(int_num)):
    sum_num_for += [int_num[i] ** 2]
print(sum_num_for)

