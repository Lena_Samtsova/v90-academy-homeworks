# 3. Дан список чисел, необходимо удалить все вхождения числа 20 из него. Результаты вывести на экран.

list_int = [1, 20, 3, 1, 20, 3, 4, 20, 3, 4]
print(list_int)
num_del = 20

while num_del in list_int:
    list_int.remove(20)
print(list_int)
