# 5. Дан список. Необходимо поменять местами самый большой и самый маленький элементы списка. Результаты вывести на экран.

list_b_s = list(input())
print(list_b_s)
i_max = list_b_s.index(max(list_b_s))
i_min = list_b_s.index(min(list_b_s))
list_b_s[i_max], list_b_s[i_min] = list_b_s[i_min], list_b_s[i_max]
print(list_b_s)
