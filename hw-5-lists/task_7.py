# 7. OK Дан список. Необходимо найти сумму и произведение элементов списка. Результаты вывести на экран.

while True:
    try:
        int_num = list(map(int, input('Введите список из чисел, через пробел: ' ).strip().split(' ')))
    except ValueError:
        print("Нужен список из чисел, через пробел. Попробуйте ещё раз...:")
        continue
    break
print(int_num)
sum_num = 0
multiplication_num = 1
for i in int_num:
    sum_num += i
    multiplication_num *= i
print(sum_num, multiplication_num)

sum_num_while = 0
multiplication_num_while = 1
index = 0
while index < len(int_num):
    sum_num_while += int_num[index]
    multiplication_num_while *= int_num[index]
    index += 1
print(sum_num_while, multiplication_num_while)