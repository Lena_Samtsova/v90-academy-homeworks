# 9. Дан список чисел. Отсортировать числа по возрастанию по алгоритму Bubble Sort. Результаты вывести на экран.

list_buble = list(map(int, input()))
print(list_buble)

swapped = True
while swapped:
    swapped = False
    for i in range(len(list_buble) -1):
        if list_buble[i] > list_buble[i + 1]:
            list_buble[i], list_buble[i + 1] = list_buble[i + 1], list_buble[i]
            swapped = True
print(list_buble)