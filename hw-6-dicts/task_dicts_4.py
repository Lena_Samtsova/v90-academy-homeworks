# 4. Даны два списка одинаковой длины. Необходимо создать из них словарь таким образом,
# чтобы элементы первого списка были ключами, а элементы второго — соответственно значениями нашего словаря.
# Результаты выведите на экран.

# list_1_itput = list(input('Enter the first list to create a dictionary: '))
# list_2_itput = list(input('Enter the second list to create a dictionary: '))
list_1_itput = ['a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l']
list_2_itput = ['1', '2', '3', '4', '5', '6', '7', '8', '9']
print(f'first list: {list_1_itput}')
print(f'second list: {list_2_itput}')
dictionary_lists = dict(zip(list_1_itput, list_2_itput))
print(f'Your dictionary is ready: {dictionary_lists}')