# 1. Имеется список с произвольными данными. Необходимо преобразовать его в множество.
# # Если какие-то элементы нельзя хешировать, то пропускаем их. Результаты выведите на экран.

# from collections.abc import Hashable
# arr_list_for_set = list(input('Enter list data, space separated: '))
# # arr_list_for_set = [1, [2], 55, 55, {1, 2, 3}, (2, 2), 'string', 5.11]
# print(f'Список с данными: {arr_list_for_set}')
# def list_for_set(lst):
#     lst = {item for item in lst if isinstance(item, Hashable)}
#     print(f'Преобразованный список с данными в множество {lst}')
# list_for_set(arr_list_for_set) #вызов функции: def list_for_set(lst)
# # как записать полученное множество в переменную?

arr_list_for_set = [1, [2], 55, 55, {1, 2, 3}, (2, 2), 'string', 5.11]
try:
    set_from_list = set(arr_list_for_set )
except TypeError:
    pass
    print(set_from_list)