# OK 4. Предоставлен список натуральных чисел. Требуется сформировать
# из них множество. Если какое-либо число повторяется,
# то преобразовать его в строку по образцу: например, если число 4 повторяется 3 раза,
# то в множестве будет следующая запись: само число 4, строка «4 4»
# (второе повторение, т.е. число дублируется в строке), строка «4 4 4»
# (третье повторение, т.е. строка множится на 3). Результаты выведите на экран.

while True:
    try:
        list_for_set = list(map(int, (input('Введите список чисел: ').strip())))
    except ValueError:
        print("Нужно ввести список чисел. Попробуйте ещё раз...:")
        continue
        break
    else:
        print(f'Список чисел: {list_for_set}')
        break
index = 0
while index < len(list_for_set):
    count_set = list_for_set.count(list_for_set[index])
    if count_set > 1:
        list_for_set[index] = str(list_for_set[index]) * count_set
    index += 1
set_finish = set(list_for_set)
print(f'Сформированное множество: {set_finish}')

