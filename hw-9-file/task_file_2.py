# ok 2. Напишите программу, которая принимает поисковый запрос
# и выводит названия текстовых файлов, содержащих искомую подстроку.
# Все файлы располагаются в заданной директории.

import os

folder = '.'
answ = set()
search = input('Введите для поиска: ')
for filename in os.listdir(folder):
    if filename.endswith(".txt"):
        filepath = os.path.join(folder, filename)
        with open(filepath, 'r', encoding = 'utf-8') as file_search:
            for line in file_search:
                if search in line:
                    answ.add(filename)
                    break
for i in answ:
    print(i)