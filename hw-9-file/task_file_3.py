# ok 3. Имеется файл text_task_3_file.txt с текстом на латинице. Напишите программу,
# которая выводит следующую статистику по тексту:
# ● количество букв латинского алфавита;
# ● число слов;
# ● число строк.

alphabet = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']
alphabet_file = 0
words_file = 0
lines_file = 0

# file_text = open('text_task_3_file.txt', 'rt')
# for i in file_text:
#     words_file += len(i.split())
#     lines_file += 1
file_text = open('text_task_3_file.txt', 'rt')
for i in file_text:
    words_file += len(i)
    lines_file += 1
    if list(i) in alphabet:#.isalpha():
        alphabet_file += 1
print("Alphabet:", alphabet_file)
print("Words:", words_file)
print("Lines:", lines_file)

