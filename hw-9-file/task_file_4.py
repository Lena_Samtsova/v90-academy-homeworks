# ok 4. Напишите программу, которая получает на вход строку с названием
# текстового файла, и выводит на экран содержимое этого файла,
# заменяя все запрещенные слова звездочками * (количество звездочек равно количеству букв в слове).
# Запрещенные слова, разделенные символом пробела, хранятся в текстовом файле forbidden_words.txt.
# Все слова в этом файле записаны в нижнем регистре.
# Программа должна заменить запрещенные слова, где бы они ни встречались, даже в середине другого слова.
# Замена производится независимо от регистра:
# если файл forbidden_words.txt содержит запрещенное слово exam,
# то слова exam, Exam, ExaM, EXAM и exAm должны быть заменены на ****.

text = input('Введите текст: ').lower()
with open('forbidden_words.txt') as forbidden_word:
	for word in forbidden_word.read().split():
		text = text.replace(word, '*' * len(word))
print(text)

# Peter Piper picked a peck of pickled peppers.A peck of pickled peppers Peter Piper picked.If Peter Piper picked a peck of pickled peppers,Where’s the peck of pickled peppers Peter Piper picked?