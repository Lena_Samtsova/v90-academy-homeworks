# ok 5. С клавиатуры в одной строке вводится произвольное количество вещественных чисел.
# Запишите их в файл, расположив каждое число на отдельной строке.
while True:
    try:
        input_num = tuple(map(float, ((input('Введите список чисел через пробел: ').strip()).split(' '))))
        # как можно в .split(' ') вписать любой символ и любое количество кроме цифры и .(точки), это можно сделать спец символами
    except ValueError:
        print('Нужно ввести список чисел через пробел, попробуйте ещё раз ...')
        continue
        break
    else:
        print(input_num)
        file_num = open('num.txt', 'w+t')
        for i in input_num:
            file_num.write(str(i) + '\n')
        file_num.close()
    break

#проверка
# file_num = open('num.txt', 'rt')
# print(file_num.read())
# file_num.close()
