# ok 6. В файле записано стихотворение.
# Выведите его на экран, а также укажите, каких слов в нем больше:
# начинающихся на гласную или на согласную букву (регистр не учитывается)?
alphabet_vowel = ['a', 'e', 'i', 'o', 'u', 'y']
alphabet_consonant = ['b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'z']
begin_vowel = 0
begin_consonant = 0

with open('poem.txt', 'rt') as file_poem:
    read_poem = file_poem.read()
    print(read_poem)
    read_poem = read_poem.lower()
read_poem = read_poem.split()

for i in read_poem:
    if i[0] in alphabet_vowel:
        begin_vowel  += 1
    elif i[0] in alphabet_consonant:
        begin_consonant += 1
print(f'Всего слов: {len(read_poem)}\nслов начинающихся на гласную: {begin_vowel}\nслов начинающихся на согласную: {begin_consonant}')
