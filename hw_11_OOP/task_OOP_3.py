# 3. Напишите программу с классом Car. Создайте инициализатор класса Car.
# Создайте атрибуты класса Car — color (цвет), type (тип), year (год).
# Напишите пять методов.
# Первый — запуск автомобиля, при его вызове выводится сообщение «Автомобиль заведен».
# Второй — отключение автомобиля — выводит сообщение «Автомобиль заглушен».
# Третий — присвоение автомобилю года выпуска.
# Четвертый метод — присвоение автомобилю типа.
# Пятый — присвоение автомобилю цвета.

class Car:
    def __init__(self,color,type,year):
        self.color = 'color'
        self.type = 'type'
        self.year = year

    def drive(self):
        print('Автомобиль заведен')

    def not_drive(self):
        print('Автомобиль заглушен')

    def change_year(self):
        return self.year

    def change_type(self):
        return self.type

    def change_color(self):
        return self.color

my_car = Car('black', 'cab', 2020 )
print(my_car.drive())
print(my_car.not_drive())
print(my_car.change_year())
print(my_car.change_type())
print(my_car.change_color())



