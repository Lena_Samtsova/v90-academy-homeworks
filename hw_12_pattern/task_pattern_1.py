# 1.
# Напишите набор из шести операторов class для моделирования такой иерархической классификации
# с помощью наследования Python. Затем добавьте в каждый класс метод speak, выводящий уникальное сообщение,
# а в суперкласс верхнего уровня Animal — метод reply,
# который просто вызывает self. speak, чтобы запустить инструмент вывода,
# специфичный для категории, из подкласса ниже в дереве (это инициирует независимый поиск при наследовании из self).
# Наконец, удалите метод speak из класса Hacker, чтобы он выбирал стандартный метод,
# находящийся выше. Когда вы завершите, ваши классы должны работать следующим образом:
# >>> spot = Cat()
# >>> spot.reply () # Animal.reply: вызывает Cat.speak
# meow
# >>> data = Hacker () # Animal.reply: вызывает Primate.speak >>> data.reply()
# Hello world!

class Animal:
    def __init__(self, name):
        self.name = name

    def _reply(self):
        super()._reply()
        print('Это класс Животное')

class Mammal(Animal):
    pass

class Cat(Animal, Mammal):
    def speak(self):
        print('Это класс Кот')
#
#
# class Dog(Animal, Mammal):
#
# class Primat(Animal, Mammal):
#
# class Hacker():

print(Animal._reply(1))

