def itemgetter(index: 'int'=None):
    index = 3
    def get_item(tpl: 'tuple'):
        """
        Возвращает элемент из кортежа
        под индексом {index}
        """
        return tpl[index]

    return get_item


def main():
    tpl1 = tuple(range(0, 10))  # (0, 1, 2, 3, ...)
    tpl2 = tuple(range(0, 100, 10))  # (0, 10, 20, 30, ...)

    first_item_getter = itemgetter(1)
    # def first_item_getter(tpl: 'tuple'):
    #     return tpl[1]

    print(first_item_getter.__doc__)
    print(first_item_getter.__annotations__)
    print(first_item_getter.__closure__[0].cell_contents)
    # print(first_item_getter(tpl1))
    # print(first_item_getter(tpl2))


if __name__ == '__main__':
    main()