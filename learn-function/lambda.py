igor_t = ('Igor', 'b', 0, 2)
julia_t = ('Julia', 'e', 0, 12)

tickets = [igor_t, julia_t]


def main():

    business = 'e'

    print(list(
        filter(lambda item: item == business, tickets)
    ))


if __name__ == '__main__':
    main()
