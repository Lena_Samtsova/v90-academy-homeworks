d = {
    'key1': 1,
    'key2': 2,
    'nested1': {
        'key1': 3, 'key2': 4,
        'nested_nested1': {'key1': 5, 'key2': 6}
    }
}


def sum_keys(obj: 'dict') -> 'float':
    """
    Складывает int значения словаря и возвращает
    сумму. Функция рекурсивная, могут быть вложенные
    словари.
    :param obj: словарь
    :return: сумма интовых значений словаря и
             его вложенных словарей
    """
    result = 0.0
    for key, value in obj.items():
        if isinstance(value, dict):
            nested_result = sum_keys(value)
            result += nested_result
        else:
            result += value
    return result


def main():
    value = sum_keys(d)
    print(value)


if __name__ == '__main__':
    main()
