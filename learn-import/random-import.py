"""
Демонстрация моделя random
"""
import random


# `random.randrange(n)` - выбор случайного значения из `range(n)`
n = 15
print('range(n):', list(range(n)))
print('randrange(n):', random.randrange(n))
print()

# `random.randint(a, b)` - генерация случайного `int`
# в диапазоне [a, b] (a и b включены!)
print('randint(a, b):', random.randint(0, 5))
print()

# генерация случайного списка целых чисел со случайно длиной
rand_list_len = random.randint(10, 100)  # случайная длина списка
                                         # от 10 до 100 (включительно)
max_value = 100  # максимальное возможное значение в случайном списке
min_value = -100  # минимальное возможное значение в случайном списке
l = [random.randint(min_value, max_value) for _ in range(rand_list_len)]
print('randomly generated int list:', l)
print()

# `random.random()` - генерация случайного float значения
# в диапазоне [0.0, 1.0) (0.0 - включительно, 1.0 - НЕТ!)
print('random():', random.random())
print()

# `random.uniform()` - (равномерное распредиление от a до b),
# генерация случайного float значения
# в диапазоне [a, b] (как `random.randint(a, b)`, только для float)
print('uniform(a, b):', random.uniform(100, 1000))
print()

# генерация случайного списка дробных чисел со случайно длиной
rand_list_len = random.randint(10, 100)  # случайная длина списка
                                         # от 10 до 100 (включительно)
max_value = 49.99  # максимальное возможное значение в случайном списке
min_value = -49.99  # минимальное возможное значение в случайном списке
l = [random.uniform(min_value, max_value) for _ in range(rand_list_len)]
print('randomly generated float list:', l)
print()

# `random.choice(iterable)` - выбор случайного элемента
# из итерируемого объекта, который поддерживает ИНДЕКСЫ
# (список, кортеж, строка, НО НЕ словарь, множество)
string = 'Hello, World!'
print('choice(str):', random.choice(string))

lst = ['Fibonacci', 'numbers', 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 'enough']
print('choice(list):', random.choice(lst))

range_object = range(n)
print('choice(range(n)):', random.choice(range_object))  # эквивалентно random.randrange(n)

# dct = {'key1': 0, 'key2': 'val2'}
# print('choice(dict):', random.choice(dct))  # НЕ БУДЕТ РАБОТАТЬ

print()

# `random.choices(iterable, *, k=1)` - работает аналогично `choice(iterable)`,
# но возвращает список с несколькими случайно выбранными значениями из
# iterable, принимет дополнительный ключевой аргумент `k` (по умолчанию =1),
# который указывет, сколько значений нужно выбрать
print('choices(str, k=5):', random.choices(string, k=5))

print('choices(list, k=7):', random.choices(string, k=7))

print()

# генерация случайной строки
rand_str_len = random.randint(10, 100)  # случайная длина списка
letters = 'абвгдеёжзийклмнопрстуфхцчшщьыъэюя ,.-'
rand_s1 = ''.join(random.choice(letters) for _ in range(rand_str_len))
# или
rand_s2 = ''.join(random.choices(letters, k=rand_str_len))
print('randomly generated string 1:', rand_s1)
print('randomly generated string 2:', rand_s2)
print()

# `random.shuffle(mutable_iterable)` - случайным образом мешает ИЗМЕНЯЕМУЮ
# последовательность.
a = [1, 2, 3, 4, 5, 6, 7, 8, 9]
print('before `shuffle(list)`:', a)
random.shuffle(a)
print('after `shuffle(list)`:', a)
print()

# `random.sample(iterable, k)` - выбор случайно перемешанного
# отрезка длиной k из iterable.
# Если k задать len(iterable), то получится аналог shuffle(iterable),
# который не изменяет изначальный объект, а создаёт новый список
a = tuple(i for i in range(10))
print('before `sample(a, k=len(a)`:', a)
b = random.sample(a, k=len(a))
print('after `sample(a, k=len(a)`:', a)
print('result of `sample(a, k=len(a)`:', b)
print()
