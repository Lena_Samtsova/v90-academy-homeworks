import string
from random import choice


letters = string.ascii_letters + string.digits
random_string = ''.join(choice(letters) for _ in range(100))
print(random_string)
