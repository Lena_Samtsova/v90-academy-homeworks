# a) 1, 2, 3, 4, . . .
# b) 0, 5, 10, 15, . . .
# c) 1, 1, 1, 1, . . .
# d) 1, −1, 1, −1, . . .
# a = 4
# for i in range(a):
#     print(i + 1)
# b = 16
# for i in range(0,b,5):
#     print(i)
# c = 5
# for i in range(c):
#     print('1')
# d = 2
# for i in range(d):
#     print('1')
#     print('-1')
                # e) 1, −2, 3, −4, 5, −6 . . .
                # e = 6
                # for i in range(6):
                #     print(i +1)
                # f) 2, 4, 8, 16, . . .
                # f = 16
                # for i in range(0,f,2):
                #     print(2 ** (i + 1))
                # # g) 2, 4, 16, 256, . . .
                # f = 256
                # for i in range(0,f,2):
                #     print(2 ** (i + 1))
                # h) 0, 1, 2, 3, 0, 1, 2, 3, 0, . . .
                # i) 1!, 3!, 5!, 7!, . . .

#
# x = 1
# for i in range(1 , 6):
#     x = 1 - 0.75 * x ** 2
# print( x)
#
#
# m = int(input())
# p = int(input())
# n = int(input())
# for i in range(n):
#     print(i + 1, m)
#     m += m * p / 100

#   Когда цикл впервые начинает работу Python устанавливает значение переменной цикла i = 0.      <---I
#   Каждый раз когда мы повторяем тело цикла (i) Python увеличивает значение переменной на 1.         I
#   n - кол-во повторений. Оно всегда постоянно(const) и зависит от числа которое мы задали.          I
#   цикл for формируется с самого начала и при итеррациях в нем изменений не происходит.
#   Например n - будет менятьсо внутри цикла, но не будет меняться голове цикла
# Например: n = 3, тогда будет сделано 3 повторения и значение переменной i всегда начнет цикл с 0    I
''' n    i                                                                                            I
    3    0    1 повторение    (3-0) распечатает *** __________________________________________________I
    3    1    2 повторение    (3-1) распечатает **
    3    2    3 повторение    (3-2) распечатает *
'''
# n = int(input())
# for i in range(n):
#     print('*' * (n - i))  # уменьшает кол-во звездочек согласно вышеизложеннного
#
#
# a = 5, 30, 4, 5
# b = a[:]
# print(a, id(a))
# print(b, id(b))
# b = 7, 8, 9
# print(a, id(a))
# print(b, id(b))
#
# marks = 4, 5, 3, 6, 4
# print(sum(marks) / len(marks))
#
# s = input().upper().strip().replace('O','a').center(25,'-')
# print(s, type(s))
#
# scores = [54,67,48,99,27]
#
# for i in range(len(scores)):
#    print(i, scores[i])
# print()
# for i, score in enumerate(scores):
#    print(i, score)
# print()
#
# for score in enumerate(scores):
#    print(score)
# print()
#
# for i, score in enumerate(scores, 1): #задали число с которого начинаем нумеравать элементы
#    print(i, score)
# print()
#
# list_a = [1, 2020, 70]
# list_b = [2, 4, 7, 2000]
# list_c = [3, 70, 7]
#
# for a in list_a:
#     for b in list_b:
#         for c in list_c:
#             if a + b + c == 2077:
#                 print(a, b, c)
#
# numbers = [5, 10, 15, 25]
# print(numbers[::-1])
#
# def bubble_sort(nums):
#     # Устанавливаем swapped в True, чтобы цикл запустился хотя бы один раз
#     swapped = True
#     while swapped:
#         swapped = False
#         for i in range(len(nums) - 1):
#             if nums[i] > nums[i + 1]:
#                 # Меняем элементы
#                 nums[i], nums[i + 1] = nums[i + 1], nums[i]
#                 # Устанавливаем swapped в True для следующей итерации
#                 swapped = True
#
# # Проверяем, что оно работает
# random_list_of_nums = [5, 2, 1, 8, 4]
# bubble_sort(random_list_of_nums)
# print(random_list_of_nums)
#
#
#
#
#
# #
# # from string import ascii_letters, digits, whitespace
# # string_1 = list(input('Enter list: '))
# # string_2 = list(input('Enter list: '))
# # string_1 += string_2
# #
# # letters_list = 0
# # digits_list = 0
# # whitespaces_list = 0
# # symbols_list = 0
#
# for i in string_1:
#     if i in ascii_letters:
#         letters_list += 1
#     elif i in digits:
#         digits_list += 1
#     elif i in whitespace:
#         whitespaces_list += 1
#     else:
#         symbols_list += 1
#
# print("Amount of letters", letters_list)
# print("Amount of digits", digits_list)
# print("Amount of whitespaces", whitespaces_list)
# print("Amount of another symbols", symbols_list)
# print(f'A new general list is formed from the two entered: {string_1}. '
#       f'Amount of letters: {letters_list}, amount of digits: {digits_list}, '
#       f'amount of whitespaces; {whitespaces_list}, amount of another symbols: {symbols_list}.'
# )

# count = 5
# while count > 0:
#     count -= 1
#     if count == 3:
#         continue
#     print (count)

number = 23
running = True
while running:
    number = 23
    guess = int(input('Введите целое число : '))
    if guess == number:
        print('Поздравляю, вы угадали.')
        running = False # это останавливает цикл while
    elif guess < number:
        print('Нет, загаданное число немного больше этого.')
    else:
        print('Нет, загаданное число немного меньше этого.')
else:
    print('Цикл while закончен.')
# Здесь можете выполнить всё что вам ещё нужно
print('Завершение.')




for i in range(1, 5): #for i in range(1,5) эквивалентно for i in [1, 2, 3, 4],
print(i)
else: # блок else не обязателен. Если он присутствует, он всегда выполняется одинраз после окончания цикла for, если только не указан оператор break.
print('Цикл for закончен')