# Дан список чисел. Вычислить сумму всех
# чисел: с for-циклом, с while-циклом.
# Результаты вывести на экран.
nums_list = [1, 2, 3, 4, 10, 11, -1, 2.4, 10]
print("Эталонная сумма: ", sum(nums_list))

# Плохой и НЕРАБОЧИЙ вариант
total = 0
total += nums_list[0]
total += nums_list[1]
total += nums_list[2]
total += nums_list[3]
total += nums_list[4]
total += nums_list[5]
total += nums_list[6]
total += nums_list[7]
print('Сумма построчно: ', total)


total_1 = 0
print(list(range(len(nums_list))))
for i in range(len(nums_list)):
    # i = get next element from range(len(nums_list))
    print(i)
    total_1 += nums_list[i]
print('Цикл "for i in range(len(nums_list))" завершён. Сумма: ', total_1)

total_2 = 0
for i2 in nums_list:
    # i2 = get next element from nums_list
    print(i2)
    total_2 += i2  # аналог `nums_list[i]` в прошлом цикле
print('Цикл "for i2 in nums_list" завершён. Сумма: ', total_2)

total_3 = 0
index = 0
while index < len(nums_list):
    total_3 += nums_list[index]
    index += 1
print('Цикл "while index < len(nums_list)" завершён. Сумма: ', total_3)
