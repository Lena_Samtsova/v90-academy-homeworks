# poem = '''\
# Программировать весело.
# Если работа скучна,
# Чтобы придать ей весёлый тон -
# используй Python!
# '''
# a = open('poem.txt', 'w') # открываем для записи (writing)
# a.write(poem) # записываем текст в файл
# a.close() # закрываем файл
# a = open('poem.txt') # если не указан режим, по умолчанию подразумевается
#
# # режим чтения ('r'eading)
#
# while True:
#     line = a.readline()
#     if len(line) == 0: # Нулевая длина обозначает конец файла (EOF)
#         break
#     print(line, end='')
# a.close() # закрываем файл

import os
work_dir = os.getcwd()
print(work_dir)
dir = 'v90-academy-homeworks/learn/file_learn.py'
print(dir)

# import os
# os.mkdir('new_file_learn')

print("Все папки и файлы:", os.listdir('v90-academy-homeworks'))

