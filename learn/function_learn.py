# def sayHello():
#     print('Привет, Мир!') # блок, принадлежащий функции
# # Конец функции
# sayHello() # вызов функции
# sayHello() # ещё один вызов функции

# def example_function_with_mutable_default(
#     kwarg=[],
# ):
#     kwarg.append(1)
#     print(kwarg, '\n')
#
# example_function_with_mutable_default()
# example_function_with_mutable_default()
#
# def example_function_with_immutable_default(
#     kwarg=None,
# ):
#     kwarg = kwarg or []
#     kwarg.append(1)
#     print(kwarg, '\n')
#
# example_function_with_immutable_default()
# example_function_with_immutable_default()
# example_function_with_immutable_default([1, 2, 3])

help(print)