# norm_input = input('Enter the string: ')
# print(norm_input, type(norm_input))
# int_input = int(input('Enter the number: '))
# # будет одно целое число без запятой
# print(int_input, type(int_input))
# float_input = float(input('Enter the number with a comma: '))
# # если будет целое число без запятой, то все равно будет float с 0 после запятой
# print(float_input, type(float_input))
# strip_space_input = input('Enter the string: ').strip()
# # можно удалять не только пробелы, тогда указать все символы в скобках(перечисления без запятой)
# print(strip_space_input, type(strip_space_input))

##################### list #########################
# list_norm_input = list(input()) #разобъет все введенное посимвольно
# print(list_norm_input, type(list_norm_input))

# list_norm_int_input = list(map(int, (input('Введите список чисел: ').strip())))
# #разобъет все по натуральным числам и уберет пробелы по краям
# print(list_norm_int_input, type(list_norm_int_input))

# list_through_symbol_int_input = list(map(int, input().split(' ')))
# # для списков состоящих из многозначных чисел;
# # в .split('')вставляем разделитель для элементов списка из чисел
# print(list_through_symbol_int_input, type(list_through_symbol_int_input))

# list_through_symbol_without_space_input = list(map(int, ((input('Введите список чисел через пробел: ').strip()).split(' '))))
# # для списков состоящих из многозначных чисел;
# # .strip() уберет пробелы по краям
# # в .split('')вставляем разделитель для элементов списка из чисел
# print(list_through_symbol_without_space_input, type(list_through_symbol_without_space_input))

# list_through_symbol_input = list(input().split(' '))
# # для списков состоящих не из одного символа; в .split('')вставляем разделитель для элементов списка
# print(list_through_symbol_input, type(list_through_symbol_input))

################### list(range(start, stop[, step])) ###################
# list_range_stop_input = list(range(int(input())))
# # ввести число, которым закончися список начинающийся
# с 0 и заканчивающися числом предшествующим введенному
# print(list_range_stop_input, type(list_range_stop_input))





# norm_input = tuple(map(int, input('Enter average temperatures for the last few days: ').split(' ')))







# на потом:
# list_range_input = list(range(list(map(int, input().split(' ')))))
# # ввести число, которым закончися список начинающийся с 0 и заканчивающися число предшествующим введенному
# # (можно модефицировать добавив шаг или изменив начало и конец range)
# print(list_range_input, type(list_range_input))




# stping_with_empty = ['', '', 'd', 'f', 'k', '', 'h', '', 'h', 'h', 'j', 'k']
# print(list(filter(None, stping_with_empty)))
# # Если функция отсутствует (None), то все элементы, которые являются ложными, будут удалены.
# # Пустая строка (пробел не пустая строка) считается ложным в Python и будет отфильтроваться.

import re
list_through_symbol_int_input = list(map(int, re.split('[^\d]+', input())))
# для списков состоящих из многозначных чисел;
# в .split('')вставляем разделитель для элементов списка из чисел
print(list_through_symbol_int_input, type(list_through_symbol_int_input))