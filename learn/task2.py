# 1. Пользователь вводит сторону квадрата, необходимо рассчитать площадь квадрата, периметр и длину диагонали, после чего вывести это все на экран.
a = float(input("Введите сторону квадрата в см:"))
if a <= 0:  ### ЗАМЕЧАНИЕ: если он второй раз введёт не правильно, то пример пропускается. И так во всех примерах далее.
    print("Введите верную сторону квадрата!")
    a = float(input("Введите сторону квадрата в см:"))
else:
    print("Площадь квадрата равна:", a ** 2)
    print("Периметр квадрата равен:", 4 * a)
    print("Длинна диагонали квадрата равна:", round(a * 2 ** 0.5))
# 2. Пользователь вводит длины двух катетов, необходимо найти длину гипотенузы и вывести ее на экран.
cat_1 = float(input("Введите длинну первого катета:"))
cat_2 = float(input("Введите длинну второго катета:"))
if cat_1 <= 0 or cat_2 <= 0:
    print("Введите верную длинну!")
    cat1 = float(input("Введите длинну первого катета:"))
    cat2 = float(input("Введите длинну второго катета:"))
else:
    print("Гипотенуза:", (cat_1 ** 2 + cat_2 ** 2) ** 0.5)
# 3. Пользователь вводит три стороны треугольника, необходимо по формуле Герона должны рассчитать его площадь и вывести на экран.
side_1 = float(input("Введите первую сторону треугольника:"))
side_2 = float(input("Введите вторую сторону треугольника:"))
side_3 = float(input("Введите третью сторону треугольника:"))
if side_1 <= 0 or side_2 <= 0 or side_3 <= 0:
    print("Введите верную сторону треугольника!")
    side_1 = float(input("Введите первую сторону треугольника:"))
    side_2 = float(input("Введите вторую сторону треугольника:"))
    side_3 = float(input("Введите третью сторону треугольника:"))
else:
    perimetr = (side_1 + side_2 + side_3)
    print("Площадь треугольника:", ((perimetr / 2) * ((perimetr / 2) - side_1) * ((perimetr / 2) - side_2) * ((perimetr / 2) - side_3)) ** 0.5)
# 4. Пользователь вводит трехзначное число. Необходимо найти сумму цифр и вывести ее на экран.
number_3 = int(input("Введите трехзначное число:"))
if number_3 <= 0:
    print("Введите верно!")
    number_3 = int(input("Введите трехзначное число:"))
elif number_3 >= 1000:
    print("Введите верно!")
    number_3 = int(input("Введите трехзначное число:"))
else:
    print("Сумма чисел:", number_3 // 100 + ((number_3 - ((number_3 // 100) * 100)) // 10) + number_3 % 10)  ### Предложение: ((number_3 - ((number_3 // 100) * 100)) // 10) == (number_3 // 10) % 10 == number_3 // 10 % 10
    ### В целом есть предложение:
    ## digit_sum = 0
    ## digit_sum += number_3 % 10
    ## number_3 //= 10
    ## digit_sum += number_3 % 10
    ## number_3 //= 10
    ## digit_sum += number_3  # heading digit is last one
    ## print("Сумма цифр:", digit_sum)
# 5. Пользователь вводит количество секунд. Необходимо рассчитать сколько дней, часов, минут и секунд соответствуют данному количеству секунд и вывести в формате дни:часы:минуты:секунды.
seconds = int(input("Введите количество секунд:"))
if seconds <= 0:
    print("Введите верно!")
    seconds = int(input("Введите количество секунд:"))
else:
    print("Время на выполнение:", seconds // 3600, ":", (seconds - ((seconds // 3600) * 3600)) // 60, ":", seconds % 60)  ### Предложение: (seconds - ((seconds // 3600) * 3600)) // 60 == seconds // 60 % 60
# 6. Пользователь вводит два числа: делимое и делитель. Вам необходимо найти целое и остаток от деления, не используя операторы % и //, и вывести их на экран. (Результаты должны быть такие, как при использовании % и //)
number_1 = int(input("Введите делимое:"))
number_2 = int(input("Введите делитель:"))
if number_2 == 0:
    print("Делить на ноль нельзя!")
else:
    print("Результат:", int(number_1 / number_2), ",", (number_1 - int(number_1 / number_2) * number_2))
    print("Результат:", divmod(number_1, number_2))
# 7. Пользователь вводит год, выяснить, является ли год високосным, и вывести сообщение на экран.
num_year = int (input("Введите год:"))
num_year = num_year % 4  ### Замечание: не совсем правильно. В соответствии с григорианским календарем, год является високосным, если его номер кратен 4, но не кратен 100, а также если он кратен 400
if num_year == 0:
    print("Год высокосный")
else:
    print("Год не высокосный")
# 8. Пользователь вводит номер месяца (от 1 до 12), необходимо выяснить к какому времени года принадлежит месяц и вывести на экран.
while True:
    try:
        number_month = int(input("Введите номер месяца года:"))
    except:
        print("Нужно ввести число, попробуйте ещё раз ...\n")
    else:
        # ОШИБКА: этот код ВСЕГДА выдаст "Это зима" (понять почему, попрошу самостоятельно). Напомню оператор вхождения in: `a in some_list`
        if number_month == 1 or 2 or 12:
            print("Это зима")
        elif number_month == 3 or 4 or 5:
            print("Это весна")
        elif number_month == 6 or 7 or 8:
            print("Это лето")
        else:
            print("Это осень")
# 9. Пользователь вводит количество рублей, которые он кладет на вклад, и количество лет. Рассчитать прибыль, если известно, что условия
# банка 10% годовых (каждый год размер вклада увеличивается на 10%. Эти деньги прибавляются к сумме вклада, и на них в следующем году тоже будут проценты). Вывести прибыль на экран.
money = float(input("Введите сумму вклада:"))
money_push = money
year = float(input("Введите количество лет вклада:"))
proc = 0.1
for i in range(int(year // 1)):
    money += money * proc
if year == year // 1:  ### Предложение: можно обойтись и без него
    print(money - money_push)
    exit()
capitalization_time = year - (year // 1)
money = money * capitalization_time * 0.01  ### ЗАМЕЧАНИЕ: 1) потеряно +=, 2) откуда 0.01, если должен быть proc, 3) забыт print конце

