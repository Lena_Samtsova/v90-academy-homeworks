"""
Менеджмент билетов.
Пользователь должен иметь возможность вести учёт проданных билетов.

Функции:
1) продажа билета: пользователь поочерёдно указывает имя, класс билета и
желаемое место (если доступно);
2) возврат билета по номеру места;
3) смена места в пределе класса (за доплату);
4) вывод списка билетов (порядок по номеру места);
5) вывод итоговой суммы с продажи билетов.

Общие параметры программы:
1) цена на билет эконом-класса;
2) цена на билет бизнес-класса;
3) цена за смену места.

Параметры билета:
1) имя - str с именем пассажира;
2) класс - str, один из двух символов: "E" (эконом), "B" (бизнес);
3) цена продажи - Decimal;
4) номер места - int в диапазоне 1-100 (бизнес: 1-10, эконом: 11-100).

--------------------------------

Функционал пока упрощён, работаем БЕЗ меню
"""
import functools
from decimal import Decimal

# from .tickets import MAIN_MENU


# при использовании этого декоратора вы определяете только
# два метода сравнения (__eq__ или __ne__ обязательно и один на выбор),
# а остальные генерируются сами!
@functools.total_ordering
class Ticket:
    ECONOMY_SEATS = range(11, 101)  # диапазон мест эконом-класса
    BUSINESS_SEATS = range(1, 11)  # диапазон мест бизнес-класса
    ECONOMY_COST = float(Decimal('79.99'))  # цена за эконом-класс
    BUSINESS_COST = float(Decimal('139.99'))  # цена за бизнес-класс
    ECONOMY = 'E'
    BUSINESS = 'B'

    @property
    def ticket_type(self):
        if self.seat_number in self.ECONOMY_SEATS:
            return self.ECONOMY
        elif self.seat_number in self.BUSINESS_SEATS:
            return self.BUSINESS

    def __init__(self, name, seat_number):
        # print('__init__: called')
        self.name = name
        self.seat_number = seat_number

    def __del__(self):
        """Destructor"""
        # print(f'I\'m deleted: {self.as_string()}')

    def __lt__(self, other: 'Ticket'):
        if not isinstance(other, Ticket):
            raise TypeError
        return self.seat_number < other.seat_number

    def __ne__(self, other):
        if not isinstance(other, Ticket):
            raise TypeError
        return self.seat_number != other.seat_number

    def __str__(self):
        return f'{self.seat_number}-{self.name}'

    def __repr__(self):
        return f'<Ticket ({str(self)})>'


class TicketsStorage:
    def __init__(self):
        self._name_index = {}
        self._seat_index = {}

    def add(self, ticket: 'Ticket'):
        self._name_index[ticket.name] = ticket
        self._seat_index[ticket.seat_number] = ticket

    def delete(self, name: 'str' = None, seat_number: 'int' = None) -> 'Ticket':
        ticket = self.find(name, seat_number)
        del self._name_index[ticket.name]
        del self._seat_index[ticket.seat_number]
        return ticket

    def find(self, name: 'str' = None, seat_number: 'int' = None) -> 'Ticket':
        if name is None and seat_number is None:
            raise ValueError('name or seat_number required')
        if name is not None:
            return self._name_index.get(name)
        if seat_number is not None:
            return self._seat_index.get(seat_number)

    def __iter__(self):
        # return TicketsStorageIterator(self._name_index)  # сортировка по имени
        return TicketsStorageIterator(self._seat_index)  # сортировка по месту

    # def __iter__(self):
    #     """Вместо самописного итератора возвращаем генератор (генераторы - это тоже итераторы)"""
    #     return (
    #         self._name_index[key] for key in sorted(self._name_index)
    #     )

    # def __iter__(self):
    #     """Определяем __iter__ как генераторную функцию (её вызов возвращает генератор)"""
    #     for key in sorted(self._name_index):
    #         yield self._name_index[key]


class TicketsStorageIterator:
    """Самописный итератор с коллекцией и состоянием"""
    def __init__(self, name_index: 'dict'):
        self._name_ordered = tuple(  # коллекция
            name_index[key] for key in sorted(name_index)
        )
        self._current_index = 0  # состояние

    def __next__(self):
        try:
            return self._name_ordered[self._current_index]
        except IndexError:
            raise StopIteration
        finally:
            self._current_index += 1

    def __iter__(self):
        return self


class TicketsStorageIteratorWithGenerator:
    """Итератор, работающий через генератор"""
    def __init__(self, name_index: 'dict'):
        self._name_ordered = (
            name_index[key] for key in sorted(name_index)
        )

    def __next__(self):
        return next(self._name_ordered)

    def __iter__(self):
        return self


def main():
    storage = TicketsStorage()
    storage.add(Ticket('Вася', 10))
    storage.add(Ticket('Петя', 11))
    storage.add(Ticket('Маша', 12))
    storage.add(Ticket('Даша', 13))

    print('неявные вызовы __iter__ и __next__:')
    for i in storage:
        print(f'    {i}')

    print('явный вызов __iter__, явный вызов __next__ и НЕявный вызов __iter__ на ИТЕРАТОРЕ:')
    iterator = iter(storage)
    print(f'    NEXT: {next(iterator)}')
    for i in iterator:
        print(f'    {i}')

    print('самописный цикл for:')
    iterator = iter(storage)
    while True:
        try:
            i = next(iterator)
        except StopIteration:
            break
        else:
            print(f'    {i}')

    print('Поиск и удаление:')
    print('    поиск по имени Вася:', storage.find('Вася'))
    print('    поиск по месту 11:', storage.find(seat_number=11))

    print('        - удаление по имени Вася')
    storage.delete(name='Вася')

    print('    поиск по имени Вася:', storage.find('Вася'))

    print('    Проверка коллекции после удаления:')
    for i in storage:
        print(f'        {i}')
    print('FINISH')


if __name__ == '__main__':
    main()
