class Point:
    """Описание класса"""
    color = 'red' # атрибуты класса
    circle = 2 # атрибуты класса
Point.color = 'black' # смена значения атрибута класса
print(Point.color) #вывод значения атрибута класса
print((Point.__dict__)) #все атрибуты класса
print(Point.__doc__) # выводит описание класса
a = Point() # создание экземпляра класса
b = Point()
print(type(a))
print(type(a) == Point)
print(a.__dict__)
print(a.color)
a.color = 'green' # создание локального атрибута экземпляра класса
print(a.__dict__) # локальный атрибут экземпляра класса
print(a.color, b.color)

Point.type_pt = 'disc' #динамическое создание нового атрибута(свойства) класса
print(Point.type_pt)
setattr(Point, 'prop', 1) #создание нового атрибута(свойства) класса
print(Point.__dict__)
setattr(Point,'type_pt', 'square') #изменение значения атрибута
print(Point.type_pt)
# Получение атрибута класса
#getattr(Point, 'name') # выдаст Исключение так как такого атрибута нет
getattr(Point, 'name', False) # выдаст False
getattr(Point, 'color', False) # если атрибут есть то False не отработает, он здесь для проверки существования атрибута
getattr(Point, 'color')
# удаление атрибутов происходит в текущем простравнстве имен(Класс или экземпляр класса)
del Point.prop
# del Point.prop # если не существет атрибут выдаст Исключение
print(hasattr(Point, 'prop')) # проверка на налилие атрибута
print(hasattr(Point, 'color'))
delattr(Point, 'type_pt') # удалит существующие атрибут
# delattr(Point, 'type_pt') # вызовет Исключение так как атрибута уже нет он удален ранее

print(hasattr(Point, 'color'))
print(hasattr(a, 'color'))
# hasattr() указывает те атрибуты к которым мы можем обратиться через данный экземпляр класса
# и не обязательно что они есть сейчас у данного экземпляра
print(hasattr(a, 'circle'))

# ПРИМЕР: Координаты точки
c = Point
d = Point
c.x = 1 # создание локальных индивидуальных свойств объекта(экземпляра класса)
c.y = 2
d.x = 10
d.y = 20
