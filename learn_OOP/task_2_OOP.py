# Класс может содержать свойства (данные) и методы (действия)
class Point:
    color = 'red' # атрибуты класса
    circle = 2 # атрибуты класса

    def set_coords(self, x, y): # имена методов это тоже атрибуты класса
        self.x = x # Создаем 2 локальных свойства: x, y
        self.y = y
        print('вызов метода set_coords' + str(self))

    def get_coords(self):
        return (self.x, self.y)

pt = Point()
pt2 = Point()
pt.set_coords(1, 2)
pt2.set_coords(10, 20)
#Point.set_coords(pt) # благодаря self мы знаем с каким экземпляром класса
# имеем дело в непосредтсвенный момент
print(pt.__dict__)
print(pt2.__dict__)
print(pt.get_coords())
print(pt2.get_coords())
f = getattr(pt, 'get_coords')
print(f())


