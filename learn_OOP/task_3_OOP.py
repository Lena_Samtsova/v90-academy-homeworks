# Инициальзатор __init__ и финализатор __del__
class Point:
    color = 'red' # атрибуты класса
    circle = 2 # атрибуты класса

    def __init__(self, x, y): # можно добавлять значения по умолчанию x=1, y=2
        self.x = x # имена атребутов x, y
        self.y = y

    def __del__(self):
        print('удаление экземпляра' + str(self))

    def set_coords(self, x, y): # имена методов это тоже атрибуты класса
        self.x = x # Создаем 2 локальных свойства: x, y
        self.y = y
        print('вызов метода set_coords' + str(self))

    def get_coords(self):
        return (self.x, self.y)

# инициализатор без магического метода __init__
# pt = Point()
# pt.set_coords(1, 2)
# print(pt.__dict__)

pt = Point(1, 2)
print(pt.__dict__)
