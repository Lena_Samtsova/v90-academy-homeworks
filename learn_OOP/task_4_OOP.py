# # __new__
# class Point:
#     def __new__(cls, *args, **kwargs):
#         print('вызов __new__ для' + str(cls))
#         return super().__new__(cls) # мы обращаемся к object
#         # и там вызываем метод __new__ после чего вызывается метод __init__
#
#     def __init__(self, x=0, y=0):
#         print('вызов __init__ для' + str(self))
#         self.x = x
#         self.y = y
#
# pt = Point()
#
#
# Паттерн singletone
class DataBase:
    __instance = None

    def __new__(cls, *args, **kwargs):
        if cls.__instance is None:
            cls.__instance = super().__new__(cls)
        return cls.__instance

    def __del__(self):
        DataBase.__instance = None

    def __init__(self, user, psw, port):
        self.user = user
        self.psw = psw
        self.port = port

    def connect(self):
        print(f'Соединение с БД:{self.user}, {self.psw}, {self.port}')

    def close(self):
        print('закрытие соединения с БД')

    def read(self):
        return 'Данные из БД'

    def write(self, data):
        print(f'запись в БД {data}')

db = DataBase('root','1234', 80)
db = DataBase('root2','1245', 90)
