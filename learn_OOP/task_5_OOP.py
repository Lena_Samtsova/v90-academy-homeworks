class Vector:
    MIN_COORD = 0
    MAX_COORD = 100

    @classmethod
    def validate(cls, arg): # cls - ссылка на текущий класс
        return cls.MIN_COORD <= arg <= cls.MAX_COORD # вернет True/False

    def __init__(self, x, y):
        self.x = self.y = 0
        # if Vector.validate(x) and Vector.validate(y): желательно не ссылаться на название класса
        if self.validate(x) and self.validate(y):
            self.x = x
            self.y = y
            print(self.norm2(self.x, self.y))

    def get_coords(self):
        return self.x, self.y

    @staticmethod
    def norm2(x, y):
        return x*x + y*y

v = Vector(1, 10)
print(Vector.validate(5)) # вызываем метод класса
res = Vector.get_coords(v) # вызываем метод экземпляра
res = v.get_coords()
print(res)

print((Vector.norm2(2, 4)))

